```sql

-- 建立表
drop table if exists stuinfo;
create table stuinfo(
stuNO varchar(20),
stuName  varchar(20),
stuAge varchar(20),
stuAddress varchar(20),
stuSeat varchar(20),
stuSex int
);
insert into stuinfo values
 ('s2501','张秋利','20','美国硅谷','1',1),
 ('s2502','李斯文','18','湖北武汉','2',0),
 ('s2503','马文才','22','湖南长沙','3',1),
 ('s2504','欧阳俊雄','21','湖北武汉','4',0),
 ('s2505','梅超风','20','湖北武汉','5',1),
 ('s2506','陈旋风','19','美国硅谷','6',1),
 ('s2507','陈风','20','美国硅谷','6',0);
 
--  建立表
 create table stuExam(
examNO varchar(20),
stuNO varchar(20),
writtenExam varchar(20),
labExam varchar(20)
);
insert into stuExam values
('1','s2501','50','70'),
('2','s2502','60','65'),
('3','s2503','86','85'),
('4','s2504','40','80'),
('5','s2505','70','90'),
('6','s2506','85','90');


-- 1.查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
 select stuNO 学号,stuName 学生姓名,stuAge 学生年龄,stuAddress 学生地址,stuSeat 座号,stuSex 学生性别 from stuinfo;
-- 
-- 2.查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
select stuName,stuAge,stuAddress from stuinfo;
-- 
-- 3.查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字
select examNO 学号,writtenExam 笔试,labExam 机试 from stuexam;
-- 5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分这四列的信息
select stuNO 学号,writtenExam 笔试,labExam 机试,writtenExam+labExam 总分 from stuexam;
-- 6.查询学生信息表（stuInfo）中学生来自哪几个地方 
SELECT stuAddress from stuinfo GROUP BY stuAddress;
-- 
-- 7.查询学生信息表（stuInfo）中学生有哪几种年龄，并为该列取对应的中文列名
select stuAge 学生年龄 from stuinfo
group by stuAge;
-- 8.查询学生信息表（stuInfo）中前3行记录 
select * from stuinfo limit 0,3;
-- 9.查询学生信息表（stuInfo）中前4个学生的姓名和座位号
select stuNO,stuSeat from stuinfo limit 0,4;

-- 11.将地址是湖北武汉，年龄是20的学生的所有信息查询出来
select * from stuinfo where stuAddress ='湖北武汉' and stuAge='20';

-- 12.将机试成绩在60-80之间的信息查询出来，并按照机试成绩降序排列
select * from stuexam where labExam between 60 and 80 order by labExam desc;
-- 13.查询来自湖北武汉或者湖南长沙的学生的所有信息
select * from stuinfo where stuAddress='湖北武汉'or stuAddress='湖南长沙';
-- 
-- 14.查询出笔试成绩不在70-90之间的信息,并按照笔试成绩升序排列
select * from stuexam where writtenExam not between 70 and 90 order by writtenExam asc;
-- 
-- 15.查询年龄没有写的学生所有信息
select * from stuinfo where stuAge is null;
-- 16.查询年龄写了的学生所有信息
select * from stuinfo where stuAge is not null;
-- 17.查询姓张的学生信息
select * from stuinfo where stuName like '张%';
-- 18.查询学生地址中有‘湖’字的信息
select * from stuinfo where stuAddress like'%湖%';
-- 19.查询姓张但名为一个字的学生信息
select * from stuinfo where stuName like '张_';
-- 20.查询姓名中第三个字为‘俊’的学生的信息，‘俊’后面有多少个字不限制
select * from stuinfo where stuName like '__俊%';
-- 21.按学生的年龄降序显示所有学生信息
select * from stuinfo order by stuAge desc;
-- 22.按学生的年龄降序和座位号升序来显示所有学生的信息
select * from stuinfo order by stuAge desc,stuSeat asc;
-- 23显示笔试第一名的学生的考试号，学号，笔试成绩和机试成绩
select * from stuexam where writtenExam =
(SELECT max(writtenExam) FROM stuexam);
-- 24.显示机试倒数第一名的学生的考试号，学号，笔试成绩和机试成绩
select * from stuexam where writtenExam =
(SELECT min(writtenExam) FROM stuexam);
-- 25.查询每个地方的学生的平均年龄
select stuAddress,round(avg(stuAge),2) from stuinfo group by stuAddress;
-- 26.查询男女生的分别的年龄总和
SELECT sum(stuAge) from stuinfo group by stuSex;
-- 27.查询每个地方的男女生的平均年龄和年龄的总和
select stuAddress,round(avg(stuAge),2),sum(stuAge) from stuinfo group by stuAddress,stuSex;































```

