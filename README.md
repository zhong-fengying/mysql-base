# MySQL数据库基础_笔记

这就是一个普通的，22级软件04班MySQL课堂笔记

# 必看
1. 自己的笔记都放到自己的姓名的文件夹里面，
2. 笔记要求用markdown格式书写
3. 文件名格式要求：日期+空格+笔记的标题 如 `20230216 MySQL的安装和登录.md`


- 时间，你不开拓它，它就悄悄长出青苔，爬上你生命的庭院，把你一生掩埋。加油！

# 简易的命令行入门教程:

1. <font color="blue">先在自己电脑上配置好git客户端的帐号:</font>

- ```
  git config --global user.name "你的姓名"
  ```

- ```
  git config --global user.email "你的邮箱地址"
  ```

2. <font color="blue">自己电脑上创建一个 git 仓库的文件夹:</font>
       

   1. 在这个文件夹里 右键，选择 `Git Base Here` 运行git客户端 输入以下命令：

           git clone 你自己仓库的地址 

   2. 会得到一个子名字为 `mysql-base` 子文件夹

   3. **关闭**git客户端的窗口

   4. 进入`mysql-base`文件夹

   5. 创建一个 `自己座号+空格+姓名` 的文件夹，例如 `25 蔡坤坤`

   6. 打开刚创建的`25 蔡坤坤`，在里面创建自己的笔记，比如 `20230216 MySQL的安装和登录.md`

   7. 在这个文件夹里再次右键，选择 `Git Base Here` 运行git客户端 ,依次输入以下命令：

        1、添加      

        ```
        git add .
        ```

        2、备注
        
        ```
       git commit -m "本次笔记的备注内容"
       ```
       
        3、提交     
       
       ```
       git push
3. <font color="blue">提交后，登录gitee.com 查看自己的仓库里有没有这个刚提交的笔记，有的点，点一下` Pull Request` </font>

   1. `Pull Request` 是指将自己的仓库里这个笔记，合并到班级的仓库，在`Pull Request` 页面里填写一下必要信息后就可以点提交

   2. 最后在班级的`Pull Requests` 确认一下，有没有看到自己刚提交的，如果有，就等待管理员通过即可。
4. 完毕
